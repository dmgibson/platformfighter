﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speedAccel;
    public float speedDecel;
    public float TargetSpeed;
    public float WalkSpeed;
    public float RunSpeed;
    public float TargetDashSpeed;
    public float DashFrameCount;
    public float AirSpeed;
    public Animator animations;
    public float JumpVel;
    public float ShJumpVel;
    public float GrScale;
    public Vector2 WallJump;
    private Vector2 OrigWallJump;
    public bool CanWallJump;

    private bool OnWall;
    private bool CanDoubleJump;
    private float DashTimer;
    private Rigidbody2D rb;
    private Vector2 MoveSpeed;
    private float CurSpeed;

    private bool CanPhase;
    private bool flipped;
    private float LastTapTime = 0.0f;
    private readonly float tapSpeed = 0.5f;

    private bool jumpCancel = false;
    private bool jump = false;
    private bool FastFall = false;

    //private CapsuleCollider2D CapCollider;
    private BoxCollider2D col;
    private Controller control;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<BoxCollider2D>(); //GetComponent<CapsuleCollider2D>();
        control = GetComponent<Controller>();
        flipped = false;
        CanPhase = false;
        ShJumpVel = JumpVel / 2;
        rb.gravityScale = GrScale;
        OnWall = false;
        OrigWallJump = WallJump;
    }

    private void OnEnable()
    {
        CurSpeed = rb.velocity.x;
        MoveSpeed = new Vector2(CurSpeed, rb.velocity.y);
        rb.gravityScale = GrScale;

        if (transform.localScale.x < 0.0f)
        {
            flipped = true;
        }
        else
        {
            flipped = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveMe();
        //JumpFixed();
    }

    private void Update()
    {
        
        MoveInput();
        JumpInput();
    }

    private void LateUpdate()
    {
        UpdateAnimator();
    }

    void JumpInput()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (OnWall)
            {
                jump = true;

                if ((WallJump.x < transform.localScale.x && !flipped) || (WallJump.x > transform.localScale.x && flipped))
                {
                    transform.localScale = new Vector3(transform.localScale.x * -1.0f, transform.localScale.y, transform.localScale.z);
                    flipped = !flipped;
                    TargetDashSpeed *= -1.0f;
                }
            }

            else if ((control.Grounded() || (!control.Grounded() && CanDoubleJump)))
            {
                jump = true;
                if (CanDoubleJump)
                    CanDoubleJump = false;
            }
        }

        if(Input.GetButtonUp("Jump") && !control.Grounded() && CanDoubleJump)
        {
            jumpCancel = true;
        }
    }

    void MoveInput()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //Check if we're grounded

        if (control.Grounded())
        {

            //If we are and the horizontal button has been pressed, check if we're double tapping
            if (Input.GetButtonDown("Horizontal"))
            {
                if ((Time.time - LastTapTime) < tapSpeed)
                {
                    DashTimer = DashFrameCount;
                }
                LastTapTime = Time.time;
            }

            //This flips us to face the left
            if (h < 0 && !flipped)
            {
                transform.localScale = new Vector3(-1.0f, transform.localScale.y, transform.localScale.z);
                flipped = true;
                TargetDashSpeed *= -1.0f;
            }
            //Face the right
            if (h > 0 && flipped)
            {
                transform.localScale = new Vector3(1.0f, transform.localScale.y, transform.localScale.z);
                flipped = false;
                TargetDashSpeed *= -1.0f;
            }

            //If we're dashing, let's stop doing that
            if (DashTimer <= 0.0f)
            {
                HandleSpeed(h, TargetSpeed);
            }

            else if (DashTimer > 0.0f)
                CurSpeed = TargetDashSpeed;

        }

        else if (Input.GetAxisRaw("Horizontal") != 0.0f)
        {
            //CurSpeed = AirSpeed * Input.GetAxisRaw("Horizontal");
            HandleSpeed(Input.GetAxisRaw("Horizontal"), AirSpeed);
        }

        //Check for phasing
        if (Input.GetKeyDown(KeyCode.DownArrow) && CanPhase)
        {
            if ((Time.time - LastTapTime) < tapSpeed)
            {
                col.isTrigger = true;
            }
            LastTapTime = Time.time;
        }

        MoveSpeed = new Vector2(CurSpeed, v);

    }

    void JumpFixed()
    {
        if (jump)
        {
            if (OnWall)
            {
                CurSpeed = WallJump.x;
                rb.velocity = WallJump;
                rb.gravityScale = GrScale;
                jump = false;
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, JumpVel);
                FastFall = true;
                jump = false;
            }
        }

        if (jumpCancel)
        {
            if (rb.velocity.y > ShJumpVel)
            {
                rb.velocity = new Vector2(rb.velocity.x, ShJumpVel);
            }
            jumpCancel = false;
        }

        if(MoveSpeed.y < 0.0f && rb.velocity.y < 0.0f && FastFall)
        {
            rb.gravityScale *= 4;
            FastFall = false;
        }
    }

    void MoveMe()
    {
        rb.velocity = new Vector2(MoveSpeed.x, rb.velocity.y);

        if (jump)
        {
            if (OnWall)
            {
                CurSpeed = WallJump.x;
                rb.velocity = WallJump;
                rb.gravityScale = GrScale;
                jump = false;
                WallJump /= 2;
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, JumpVel);
                FastFall = true;
                jump = false;
            }
        }

        if (jumpCancel)
        {
            if (rb.velocity.y > ShJumpVel)
            {
                rb.velocity = new Vector2(rb.velocity.x, ShJumpVel);
            }
            jumpCancel = false;
        }

        if (MoveSpeed.y < 0.0f && rb.velocity.y < 0.0f && FastFall)
        {
            rb.gravityScale *= 4;
            FastFall = false;
        }

    }

    private void UpdateAnimator()
    {
        animations.SetFloat("WalkInput", Mathf.Abs(MoveSpeed.x));
        animations.SetFloat("YSpeed", rb.velocity.y);
        if (jump)
        {
            animations.SetTrigger("Jump");
        }

        if(DashTimer > 0)
        {
            DashTimer -= Time.deltaTime;
            if (DashTimer < 0)
            {
                DashTimer = 0.0f;
            }
            animations.SetFloat("DashTimer", DashTimer*100);  
        }
    }

    void HandleSpeed(float h, float target)
    {
        if (h > 0.0f)
        {
            if (CurSpeed < target * h)
                CurSpeed += speedAccel;
            else if (CurSpeed > target * h)
                CurSpeed = target;

        }
        else if (h < 0.0f)
        {
            if (CurSpeed > target * h)
                CurSpeed -= speedAccel;
            else if (CurSpeed < target * h)
                CurSpeed = -target;
        }
        else if (h == 0.0f)
        {
            if (CurSpeed > 0.0f)
            {
                CurSpeed -= speedDecel;
                if (CurSpeed < 0.0f)
                    CurSpeed = 0.0f;
            }
            else if (CurSpeed < 0.0f)
            {
                CurSpeed += speedDecel;
                if (CurSpeed > 0.0f)
                    CurSpeed = 0.0f;
            }
        }
    }

   

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Phase_Through") && col.isTrigger)
        {
            control.SetGrounded(false);
            col.isTrigger = false;
            CanPhase = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            control.SetGrounded(true);
            CanPhase = false;
            col.isTrigger = false;
        }
    }

    public void SetGrounded()
    {
        //control.setGrounded(true);
        CurSpeed = 0.0f;
        CanDoubleJump = false;
        WallJump = OrigWallJump;
        if (rb.gravityScale != GrScale)
            rb.gravityScale = GrScale;
    }

    public void HitTop()
    {
        MoveSpeed.y = 0.0f;
    }

    public void SetDoubleJump(bool Jump)
    {
        CanDoubleJump = Jump;
    }

    public void SetCanPhase(bool phase)
    {
        CanPhase = phase;
    }

    public void HitWall(int Direction)
    {
        // If Direction = 1, hit on front, if Direction = -1, hit on back

        if ((Direction == 1 && transform.localScale.x == 1.0f) || (Direction == -1 && transform.localScale.x == -1.0f))
        {
            WallJump.x = OrigWallJump.x * -1.0f;
        }

        else if( (Direction == 1 && transform.localScale.x == -1.0f) || (Direction == -1 && transform.localScale.x == 1.0f))
        {
            WallJump.x = OrigWallJump.x;
        }

        CurSpeed = 0.0f;
        MoveSpeed.x = 0.0f;
        
        if (CanWallJump)
        {
            OnWall = true;
        }
    }
    public void LeaveWall()
    {
        //CurSpeed = rb.velocity.x;
        //MoveSpeed.x = CurSpeed;
        OnWall = false;
    }


}
