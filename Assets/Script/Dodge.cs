﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : MonoBehaviour
{
    public float StartSpeed;
    public float Decel;
    public float DecelTarget;
    public float DodgeLength;
    public float IFrameStart, IFrameEnd;

    public float DodgeTimer;
    private Vector2 MoveSpeed;
    private Rigidbody2D rb;
    private Controller control;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        control = GetComponent<Controller>();
        MoveSpeed = Vector2.zero;
        DodgeTimer = 0.0f;
    }

    private void OnEnable()
    {
        DodgeTimer = 0.0f;
        ReadInput();
        StartDodge();
    }

    private void Update()
    {
        if(DodgeTimer > 0.0f)
        {

            if (DodgeTimer >= (DodgeLength - IFrameStart))
                ProcessDash();

            DodgeTimer -= Time.deltaTime;

            if (DodgeTimer == (DodgeLength - IFrameStart))
            {
                control.setInvin(true);
                print("Invincible");
            }

            else if (DodgeTimer == (DodgeLength - IFrameEnd))
            {
                control.setInvin(false);
                print("Not Invincible");
                //MoveSpeed = Vector2.zero;
            }
            

            if (DodgeTimer <= 0.0f)
            {
                DodgeTimer = 0.0f;
                control.SetDodge(false);
                
            }
        }

            
    }

    private void ReadInput()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        MoveSpeed = new Vector2(h, v)*StartSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (DodgeTimer >= (DodgeTimer - IFrameEnd))
        //if(DodgeTimer == DodgeLength)
        if (DodgeTimer >= DodgeLength/2)
        {
            rb.velocity = MoveSpeed;
        }
    }

    private void ProcessDash()
    {

        if (MoveSpeed.x > 0.0f && MoveSpeed.x > DecelTarget)
        {
            MoveSpeed.x -= Decel;
            if (MoveSpeed.x < DecelTarget)
                MoveSpeed.x = DecelTarget;
        }
        else if (MoveSpeed.x < 0 && MoveSpeed.x < -DecelTarget)
        {
            MoveSpeed.x += Decel;
            if (MoveSpeed.x > -DecelTarget)
                MoveSpeed.x = -DecelTarget;
        }

        if (MoveSpeed.y > 0 && MoveSpeed.y > DecelTarget)
        {
            MoveSpeed.y -= Decel;
            if (MoveSpeed.y < DecelTarget)
                MoveSpeed.y = DecelTarget;
        }
        else if (MoveSpeed.y < 0)
        {
            MoveSpeed.y += Decel;
            if (MoveSpeed.y > -DecelTarget)
                MoveSpeed.y = -DecelTarget;
        }
    }     
    

    public void StartDodge()
    {
        DodgeTimer = DodgeLength;
    }


    public bool IsDodging()
    {
        if (DodgeTimer > 0.0f)
            return true;
        else
            return false;
    }
}
