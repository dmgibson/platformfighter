﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    // The controller controls when to switch from each script, as such decides when the character is grounded

    public bool CanWallJump, CanWallCling;
    public string DInput;
    public float LandLag;
    public Transform TCheck, GCheck, FCheck, BCheck;
    public LayerMask GLayer;

    private float LandLagTimer;
    public bool isInvin, helpless, grounded;
    private Movement move;
    private Dodge dodge;

    void Awake()
    {
        move = GetComponent<Movement>();
        dodge = GetComponent<Dodge>();

        grounded = false;

        move.enabled = true;
        dodge.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(LandLagTimer > 0.0f)
        {
            LandLagTimer -= Time.deltaTime;

            if(LandLagTimer <= 0.0f)
            {
                SetGrounded(true);
            }
        }

        if (Input.GetButtonDown(DInput) && !dodge.enabled && !grounded)
        {
            SetDodge(true);
            //dodge.StartDodge();
        }

        if(grounded && dodge.enabled)
        {
            SetDodge(false);
            SetWallInteract(false);
        }
    }

    public void setInvin(bool setter)
    {
        isInvin = setter;
    }

    public void SetDodge(bool val)
    {
        dodge.enabled = val;
        move.enabled = !val;
    }

    public void SetWallInteract(bool val)
    {
        move.enabled = !val;
    }

    public void SetMove(bool val)
    {
        move.enabled = val;
    }

    public void SetGrounded(bool isG)
    {
        grounded = isG;

        if (grounded)
        {
            if(dodge.enabled)
                dodge.enabled = false;
            if(!move.enabled)
                move.enabled = true;
            move.SetGrounded();
        }
    }
    public bool Grounded()
    {
        return grounded;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.02f, GLayer);
            bool tCheck = Physics2D.OverlapCircle(TCheck.position, 0.02f, GLayer);
            bool fCheck = Physics2D.OverlapCircle(FCheck.position, 0.02f, GLayer);
            bool bCheck = Physics2D.OverlapCircle(BCheck.position, 0.02f, GLayer);

            if (gCheck)
            {
                LandLagTimer = LandLag;
                SetGrounded(true);
            }
            else if (tCheck)
            {
                if (dodge.enabled)
                    dodge.enabled = false;
                if (!move.enabled)
                    move.enabled = true;
                move.HitTop();

            }

            else if ((fCheck || bCheck) && !dodge.enabled)
            {
                if (dodge.enabled)
                    dodge.enabled = false;
                if (!move.enabled)
                    move.enabled = true;

                if (fCheck)
                {
                    move.HitWall(1);
                }
                else if (bCheck)
                {
                    move.HitWall(-1);
                }

                /*if (move.enabled) 
                    move.enabled = false;*/
            }
        }

        if (collision.gameObject.CompareTag("Phase_Through"))
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.02f, GLayer);

            if (gCheck)
            {
                LandLagTimer = LandLag;
                SetGrounded(true);
                move.SetCanPhase(true);
            }

        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Phase_Through"))
        {
            bool gCheck = Physics2D.OverlapCircle(GCheck.position, 0.5f, GLayer);
            bool fCheck = Physics2D.OverlapCircle(FCheck.position, 0.5f, GLayer);
            bool bCheck = Physics2D.OverlapCircle(BCheck.position, 0.5f, GLayer);

            if (!gCheck)
            {
                SetGrounded(false);
                if (move.enabled == true)
                {
                    move.SetDoubleJump(true);
                }
            }

            if ((fCheck || bCheck) && !dodge.enabled)
            {
                if (!move.enabled) 
                    move.enabled = true;

                 move.LeaveWall();
            }

            if (collision.gameObject.CompareTag("Phase_Through"))
            {
                move.SetCanPhase(false);
            }

        }
    }

}
