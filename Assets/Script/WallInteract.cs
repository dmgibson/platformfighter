﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallInteract : MonoBehaviour
{
    public bool WallCling;
    public float WallGrav;
    public Vector2 WallJump;

    private Rigidbody2D rb;
    private bool jump;
    private float direction; // Will be equal to localscale (-1 = left, 1 = rught)

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        if (WallCling)
        {
            WallGrav = 0.0f;
        }
        direction = transform.localScale.x;

        if(direction < 0)
        {
            WallJump.x = Mathf.Abs(WallJump.x);
        }
        else
        {
            WallJump.x *= -1.0f; 
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Jump"))
        {
            transform.localScale = new Vector3(direction * -1.0f, transform.localScale.y, transform.localScale.z);
            jump = true;
        }

    }

    private void FixedUpdate()
    {
        if (jump)
        {
            rb.velocity = WallJump;
            jump = false;
        }
    }

}
